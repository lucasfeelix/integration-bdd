"""When you write your constants to help you."""

from os import getcwd
from os.path import join

exe_path = r'C:\\hello.exe'

PATH = getcwd()
LOG_FILE_DIR = join(PATH, "log")
LOGGER_CONFIG = join(PATH, "integration", "data", "logging.json")
HEAD = {
    'X-VTEX-API-AppKey': 'vtex-appkey-test',
    'X-VTEX-API-AppToken': 'vtex-apptoken-test'
}
BACKEND = 'https://cdc-manager.qa-titan.lendico.net.br'
URL_TO_REMOVE = 'https://frontend-pay-authentication.k-qa.lendico.net.br/check-costummer/'  # NOQA
