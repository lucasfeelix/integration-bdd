from json import loads

from requests import post

from integration.data.payloads import PAYMENT
from integration.helpers.constants import BACKEND, HEAD, URL_TO_REMOVE


def make_post_payment():
    req = post(f'{BACKEND}/vtex/payments', json=PAYMENT, headers=HEAD)
    token = loads(req.text)['redirectUrl'].replace(URL_TO_REMOVE, '')
    return token

