# language: pt
# encoding: utf-8

Funcionalidade: Rota Payments

  Cenário: POST para a rota payments
    Dado que tenho o seguinte payload
    """
    {
        "reference": "1234567891011121314",
        "orderId": "C565D5A632SG3S6A9S74D8A82J",
        "transactionId": "1234567890ABCDEFGHIOJKLMNOPQRSTU",
        "paymentId": "F5C1A4E20D3B4E07B7E871F5B5BC9F91",
        "paymentMethod": "Visa",
        "paymentMethodCustomCode": "123",
        "merchantName": "mystore",
        "value": 4307.23,
        "currency": "BRL",
        "installments": 3,
        "deviceFingerprint": "12ade389087fe",
        "card": {
            "holder": "Iterative QA Auto",
            "number": "4682185088924788",
            "csc": "021",
            "expiration": {
                "month": "06",
                "year": "2029"
            }
        },
        "miniCart": {
            "shippingValue": 11.44,
            "taxValue": 10.01,
            "buyer": {
                "id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
                "firstName": "Ana",
                "lastName": "Maria",
                "document": "26212412041",
                "documentType": "CPF",
                "email": "ana.maria@iterative.com",
                "phone": "+5511912345678"
            },
            "shippingAddress": {
                "country": "BRA",
                "street": "Praia de Botafogo St.",
                "number": "300",
                "complement": "3rd Floor",
                "neighborhood": "Botafogo",
                "postalCode": "22250040",
                "city": "Rio de Janeiro",
                "state": "RJ"
            },
            "billingAddress": {
                "country": "BRA",
                "street": "Brigadeiro Faria Lima Avenue",
                "number": "2000",
                "complement": "10th Floor",
                "neighborhood": "Itaim Bibi",
                "postalCode": "04538132",
                "city": "S\\u00e3o Paulo",
                "state": "SP"
            },
            "items": [{
                "id": "132981",
                "name": "My First Product",
                "price": 2134.9,
                "quantity": 2,
                "discount": 5.0
            }, {
                "id": "123242",
                "name": "My Second Product",
                "price": 21.98,
                "quantity": 1,
                "discount": 1.0
            }]
        },
        "url": "",
        "callbackUrl": "https://api.example.com/some-path/to-notify/status-changes?an=mystore",
        "returnUrl": "https://return.url.example.com/some-path"
    }
    """
    Quando efetuar um POST para o endpoint payments
    Então a API deverá retornar 200

  Cenário: PUT para a rota payments
    Dado que tenho o seguinte payload
    """
    {
        "reference": "1234567891011121314",
        "orderId": "C565D5A632SG3S6A9S74D8A82J",
        "transactionId": "1234567890ABCDEFGHIOJKLMNOPQRSTU",
        "paymentId": "F5C1A4E20D3B4E07B7E871F5B5BC9F91",
        "paymentMethod": "Visa",
        "paymentMethodCustomCode": "123",
        "merchantName": "mystore",
        "value": 4307.23,
        "currency": "BRL",
        "installments": 3,
        "deviceFingerprint": "12ade389087fe",
        "card": {
            "holder": "Iterative QA Auto",
            "number": "4682185088924788",
            "csc": "021",
            "expiration": {
                "month": "06",
                "year": "2029"
            }
        },
        "miniCart": {
            "shippingValue": 11.44,
            "taxValue": 10.01,
            "buyer": {
                "id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
                "firstName": "Ana",
                "lastName": "Maria",
                "document": "26212412041",
                "documentType": "CPF",
                "email": "ana.maria@iterative.com",
                "phone": "+5511912345678"
            },
            "shippingAddress": {
                "country": "BRA",
                "street": "Praia de Botafogo St.",
                "number": "300",
                "complement": "3rd Floor",
                "neighborhood": "Botafogo",
                "postalCode": "22250040",
                "city": "Rio de Janeiro",
                "state": "RJ"
            },
            "billingAddress": {
                "country": "BRA",
                "street": "Brigadeiro Faria Lima Avenue",
                "number": "2000",
                "complement": "10th Floor",
                "neighborhood": "Itaim Bibi",
                "postalCode": "04538132",
                "city": "S\\u00e3o Paulo",
                "state": "SP"
            },
            "items": [{
                "id": "132981",
                "name": "My First Product",
                "price": 2134.9,
                "quantity": 2,
                "discount": 5.0
            }, {
                "id": "123242",
                "name": "My Second Product",
                "price": 21.98,
                "quantity": 1,
                "discount": 1.0
            }]
        },
        "url": "",
        "callbackUrl": "https://api.example.com/some-path/to-notify/status-changes?an=mystore",
        "returnUrl": "https://return.url.example.com/some-path"
    }
    """
    Quando efetuar um PUT para o endpoint payments
    Então a API deverá retornar 405
    E o erro será
    """
    {
       "message": "The method is not allowed for the requested URL."
    }
    """

  Cenário: GET para a rota payments
    Dado que tenho o seguinte payload
    """
    {
        "reference": "1234567891011121314",
        "orderId": "C565D5A632SG3S6A9S74D8A82J",
        "transactionId": "1234567890ABCDEFGHIOJKLMNOPQRSTU",
        "paymentId": "F5C1A4E20D3B4E07B7E871F5B5BC9F91",
        "paymentMethod": "Visa",
        "paymentMethodCustomCode": "123",
        "merchantName": "mystore",
        "value": 4307.23,
        "currency": "BRL",
        "installments": 3,
        "deviceFingerprint": "12ade389087fe",
        "card": {
            "holder": "Iterative QA Auto",
            "number": "4682185088924788",
            "csc": "021",
            "expiration": {
                "month": "06",
                "year": "2029"
            }
        },
        "miniCart": {
            "shippingValue": 11.44,
            "taxValue": 10.01,
            "buyer": {
                "id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
                "firstName": "Ana",
                "lastName": "Maria",
                "document": "26212412041",
                "documentType": "CPF",
                "email": "ana.maria@iterative.com",
                "phone": "+5511912345678"
            },
            "shippingAddress": {
                "country": "BRA",
                "street": "Praia de Botafogo St.",
                "number": "300",
                "complement": "3rd Floor",
                "neighborhood": "Botafogo",
                "postalCode": "22250040",
                "city": "Rio de Janeiro",
                "state": "RJ"
            },
            "billingAddress": {
                "country": "BRA",
                "street": "Brigadeiro Faria Lima Avenue",
                "number": "2000",
                "complement": "10th Floor",
                "neighborhood": "Itaim Bibi",
                "postalCode": "04538132",
                "city": "S\\u00e3o Paulo",
                "state": "SP"
            },
            "items": [{
                "id": "132981",
                "name": "My First Product",
                "price": 2134.9,
                "quantity": 2,
                "discount": 5.0
            }, {
                "id": "123242",
                "name": "My Second Product",
                "price": 21.98,
                "quantity": 1,
                "discount": 1.0
            }]
        },
        "url": "",
        "callbackUrl": "https://api.example.com/some-path/to-notify/status-changes?an=mystore",
        "returnUrl": "https://return.url.example.com/some-path"
    }
    """
    Quando efetuar um GET para o endpoint payments
    Então a API deverá retornar 405
    E o erro será
    """
    {
       "message": "The method is not allowed for the requested URL."
    }
    """
