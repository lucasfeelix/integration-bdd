# language: pt
# encoding: utf-8

Funcionalidade: Rota Clients

  @post_payment
  Cenário: POST para a rota clients
    Dado que tenho o seguinte payload
    """
    {
        "client_id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
        "first_name": "Ana da Silva junior",
        "last_name": "Maria",
        "document": "26212412041",
        "document_type": "CPF",
        "phone": "+5511983741192",
        "email": "ana.maria@iterative.com",
        "password": "ana123!",
        "country": "BRA",
        "street": "Av. Mofarrej",
        "number": "825",
        "complement": "Galpão",
        "neighborhood": "Vila Leopoldina",
        "postal_code": "02675031",
        "city": "São Paulo",
        "state": "SP",
        "birth_date": "1996-05-22"
    }
    """
    Quando efetuar um POST para o endpoint clients
    Então a API deverá retornar 405
    E o erro será
    """
    {
       "message": "The method is not allowed for the requested URL."
    }
    """

  @post_payment
  Cenário: PUT para a rota clients
    Dado que tenho o seguinte payload
    """
    {
        "client_id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
        "first_name": "Ana da Silva junior",
        "last_name": "Maria",
        "document": "26212412041",
        "document_type": "CPF",
        "phone": "+5511983741192",
        "email": "ana.maria@iterative.com",
        "password": "ana123!",
        "country": "BRA",
        "street": "Av. Mofarrej",
        "number": "825",
        "complement": "Galpão",
        "neighborhood": "Vila Leopoldina",
        "postal_code": "02675031",
        "city": "São Paulo",
        "state": "SP",
        "birth_date": "1996-05-22"
    }
    """
    Quando efetuar um PUT para o endpoint clients
    Então a API deverá retornar 200

  @post_payment
  Cenário: GET para a rota clients
    Dado que tenho o seguinte payload
    """
    {
        "client_id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
        "first_name": "Ana da Silva junior",
        "last_name": "Maria",
        "document": "26212412041",
        "document_type": "CPF",
        "phone": "+5511983741192",
        "email": "ana.maria@iterative.com",
        "password": "ana123!",
        "country": "BRA",
        "street": "Av. Mofarrej",
        "number": "825",
        "complement": "Galpão",
        "neighborhood": "Vila Leopoldina",
        "postal_code": "02675031",
        "city": "São Paulo",
        "state": "SP",
        "birth_date": "1996-05-22"
    }
    """
    Quando efetuar um GET para o endpoint clients
    Então a API deverá retornar 405
    E o erro será
    """
    {
       "message": "The method is not allowed for the requested URL."
    }
    """

  @post_payment
  Cenário: GET para a rota clients
    Dado efetuar um GET para o endpoint clients
    Então a API deverá retornar 200
    E o response será
    """
    {
        "user": {
            "email": "ana.maria@iterative.com",
            "document": "26212412041",
            "document_type": "CPF"
        },
        "client_id": "ec7d75d2-5d93-4768-8c84-cbcde0f55377",
        "first_name": "Ana da Silva junior",
        "last_name": "Maria",
        "phone": "+5511983741192",
        "country": "BRA",
        "street": "Av. Mofarrej",
        "number": "825",
        "complement": "Galpão",
        "neighborhood": "Vila Leopoldina",
        "postal_code": "02675031",
        "city": "São Paulo",
        "state": "SP",
        "is_new_client": false
    }
    """
