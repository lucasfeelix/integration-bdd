from behave import step, when
from requests import get, post, put
from integration.helpers.constants import BACKEND, HEAD


@when(u'efetuar um POST para o endpoint clients')
def step_impl(context):
    context.requisition = post(f'{BACKEND}/api/clients', json=context.payload,
                               headers=HEAD)


@when(u'efetuar um PUT para o endpoint clients')
def step_impl(context):
    context.requisition = put(f'{BACKEND}/api/clients', json=context.payload,
                              headers=HEAD)


@step(u'efetuar um GET para o endpoint clients')
def step_impl(context):
    context.requisition = get(f'{BACKEND}/api/clients/{context.token}',
                              headers=HEAD)

