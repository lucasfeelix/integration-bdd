from behave import when
from requests import get, post, put
from integration.helpers.constants import BACKEND, HEAD


@when(u'efetuar um POST para o endpoint payments')
def step_impl(context):
    context.requisition = post(f'{BACKEND}/vtex/payments', json=context.payload,
                               headers=HEAD)


@when(u'efetuar um PUT para o endpoint payments')
def step_impl(context):
    context.requisition = put(f'{BACKEND}/vtex/payments', json=context.payload,
                              headers=HEAD)


@when(u'efetuar um GET para o endpoint payments')
def step_impl(context):
    context.requisition = get(f'{BACKEND}/vtex/payments', json=context.payload,
                              headers=HEAD)
