from json import loads

from behave import then, given


@given(u'que tenho o seguinte payload')
def step_impl(context):
    context.payload = loads(context.text)


@then(u'a API deverá retornar 200')
def step_impl(context):
    assert context.requisition.status_code == 200, f"""
        Status {context.requisition.status_code} != 200 que era esperado."""


@then(u'a API deverá retornar 405')
def step_impl(context):
    assert context.requisition.status_code == 405, f"""
        Status {context.requisition.status_code} != 405 que era esperado."""


@then(u'o erro será')
def step_impl(context):
    assert loads(context.text) == loads(context.requisition.text)


@then(u'o response será')
def step_impl(context):
    assert loads(context.text) == loads(context.requisition.text), f"""
        O response veio diferente do esperado."""


